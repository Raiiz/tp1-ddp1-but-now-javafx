# TP1 DDP1 but now JavaFX

This is a reimplementation from the program I've made with Python language to create Rotating Colorful Squares and Disks, the task from TP1 DDP1 Gasal 2019/2020 Fasilkom UI.
This implementation with JavaFX is still not good enough, since the output isn't really similar to the one that comes from TP1 DDP1 Gasal 2019/2020 Fasilkom UI.
Let me know if u have critics and ideas to improve this code!