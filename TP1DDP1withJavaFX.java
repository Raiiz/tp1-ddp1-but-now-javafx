import java.util.Random;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

public class TP1DDP1withJavaFX extends Application {
    
    // overriding javafx.application.Application.start
    @Override
    public void start(Stage stage) throws Exception {
        
        // instantiate Random and Group object
        Random rand = new Random();
        Group group = new Group();

        // for loop to create 72 "rotating colorful squares"
        for (int i = 0; i < 72; i++) {
            
            // create Rectangle object
            Rectangle rect = new Rectangle(250, 175, 40+(2*i), 40+(2*i));

            // fill the Rectangle with random colors
            rect.setFill(Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));

            // set the stroke for Rectangle to black
            rect.setStroke(Color.BLACK);

            // creating Rotate object for the "rotating" effect and include it in Rectangle
            Rotate rotate1 = new Rotate(5*i, 250, 175 + (40+(2*i)));
            rect.getTransforms().addAll(rotate1);

            // group the Rectangle created
            group.getChildren().add(rect);
        }

        // for loop to create 36 "rotating colorful disks"
        for (int j = 0; j < 36; j++) {

            // create Circle object
            Circle circ = new Circle(650, 225, 91-(2*j));

            // fill the Circle with random colors
            circ.setFill(Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));

            // set the stroke for Circle to black
            circ.setStroke(Color.BLACK);

            // creating Rotate object for the "rotating" effect and include it in Circle
            Rotate rotate2 = new Rotate((-10)*j, 650, 225 + (91-(2*j)));
            circ.getTransforms().addAll(rotate2);

            // group the Circle created
            group.getChildren().add(circ);
        }

        // create Scene as the place for Group and Scene setting
        Scene scene = new Scene(group, 800, 600);
        stage.setTitle("Rotating colorful squares and disks");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    // main method to launch JavaFX application
    public static void main(String args[]){
        launch(args);
    }
}